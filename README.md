xbmc :: plugin.video.thrasherskateboard
=======================================

XBMC video addon that allows browsing and watching videos from the Thrasher
Skateboard Magazine website.

Thrasher is a monthly skateboarding magazine, founded in 1981 by Kevin
Thatcher, Eric Swenson, and Fausto Vitello, published by the San Francisco, 
United States-based company, High Speed Productions.  The magazine also 
maintains a website that consists of numerous features, including segments, 
such as "Firing Line" and "Hall of Meat", and other video collections. 

Supported Versions of XBMC
--------------------------
* Frodo
* Gotham
